//
//  ViewController.m
//  KVC探究
//
//  Created by Nasy on 2020/6/8.
//  Copyright © 2020 nasy. All rights reserved.
//

#import "ViewController.h"
#import "YNPerson.h"



@interface ViewController ()

/// 属性
@property (nonatomic, strong) YNPerson *p;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _p = [[YNPerson alloc]init];
//    [_p addObserver:self forKeyPath:@"name" options: NSKeyValueObservingOptionNew context:nil];
//    [_p setValue:@"Nasy1" forKeyPath: @"name"];
    
    
    ///打印实例变量，查看值存储位置
//    NSLog(@"%@",_p->isName);
//    NSLog(@"%@-%@",_p->name,_p->isName);
//    NSLog(@"%@-%@-%@",_p->_isName,_p->name,_p->isName);
////    NSLog(@"%@-%@-%@-%@",_p->_name,_p->_isName,_p->name,_p->isName);
    
//    _p->_name   = @"_name";
//    _p->_isName = @"_isName";
//    _p->name    = @"name";
//    _p->isName  = @"isName";
    NSLog(@"%@",[_p valueForKey:@"name"]);
}

//-(void)dealloc {
//    [_p removeObserver:self forKeyPath:@"name"];
//}
//
//- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
//    
//    NSLog(@"*********%@", change);
//}
//

@end
