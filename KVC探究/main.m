//
//  main.m
//  KVC探究
//
//  Created by Nasy on 2020/6/8.
//  Copyright © 2020 nasy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
