//
//  AppDelegate.h
//  KVC探究
//
//  Created by Nasy on 2020/6/8.
//  Copyright © 2020 nasy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

