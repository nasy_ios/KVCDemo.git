//
//  YNPerson.m
//  KVC探究
//
//  Created by Nasy on 2020/6/8.
//  Copyright © 2020 nasy. All rights reserved.
//

#import "YNPerson.h"

@implementation YNPerson



#pragma mark - set相关
/**
 1. **赋值过程--- setValue:forKey:的默认实现**
 - 1.按照顺序查找第一个名为`set<Key>:`或`_set<Key>`的方法。如果找到, 传入value值并调用。
 - 2.如果找不到简单访问器,并且类方法`accessInstanceVariablesDirectly`返回YES, 则按以下顺序查找实例变量:` _<key>`、` _is<Key>`、`<key>`、`is<Key>` 。如果找到, 则直接使用value值设置变量并完成。
 - 3.如果找不到以上方法或实例变量,则调用`setValue:forUndefinedKey:`。默认情况下，这会引发异常，但NSObject的子类可以通过重载并根据特定key做一些特殊处理。
 
 **/
//- (void)setName:(NSString *)name{
////    _name = name;
//    NSLog(@"调用了%s方法",__func__);
//    
//}

//- (void)_setName:(NSString *)name{
////    _name = name;
//    NSLog(@"调用了%s方法",__func__);
//}


//
//- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
//
//    @try {
//        NSLog(@"调用了%s方法",__func__);
//        [super setValue:value forUndefinedKey:key];
//    } @catch (NSException *exception) {
//
//        NSLog(@"%@",exception);
//    } @finally {
//
//    }
//}


//- (void)willChangeValueForKey:(NSString *)key{
//    
//    NSLog(@"%s---%@",__func__,key);
//    [super willChangeValueForKey:key];
//}
//
//- (void)didChangeValueForKey:(NSString *)key {
//    NSLog(@"%s---%@",__func__,key);
//    [super didChangeValueForKey:key];
//
//}

#pragma mark - get相关
/**
 **取值过程--- valueForKey：的默认实现**
 - 1.按照顺序搜索名称为`get<Key>`、`<key>`、`is<Key>`或`_<key>`的第一个访问器方法。如果找到，调用它并继续到步骤3。否则请继续执行下一步。
 - 2.如果找不到简单访问器方法,并且消息接收者的类方法`accessInstanceVariablesDirectly`返回YES,则系统按以下顺序搜索名为:`_<key>`、`_is<Key>`、`<key>`或`is<Key>`的实例变量。如果找到,则直接获取实例变量的值,然后继续执行步骤3。否则, 继续跳转到步骤4。
 - 3.如果获取到的属性值是对象指针,即获取的是对象,则直接将对象返回。如果获取到的属性值是NSNumber支持的数据类型,则将其存储在NSNumber实例并返回。如果获取到的属性值不是 NSNumber 支持的类型, 则转换为NSValue对象, 然后返回。
 - 4.如果上述所有方法都没有执行，则调用`valueForUndefinedKey：`。默认情况下，这会引发异常，但NSObject的子类可以通过重载并根据特定key做一些特殊处理。
 **/
//- (NSString *)getName{
//    NSLog(@"调用了%s方法",__func__);
//    return NSStringFromSelector(_cmd);
//}
//
//- (NSString *)name{
//    NSLog(@"调用了%s方法",__func__);
//    return NSStringFromSelector(_cmd);
//}
//- (NSString *)isName{
//    NSLog(@"调用了%s方法",__func__);
//    return NSStringFromSelector(_cmd);
//}
//- (NSString *)_name{
//    NSLog(@"调用了%s方法",__func__);
//    return NSStringFromSelector(_cmd);
//}

//- (id)valueForUndefinedKey:(NSString *)key {
//    @try {
//         NSLog(@"%s",__func__);
//        [super valueForUndefinedKey:key];
//    } @catch (NSException *exception) {
//        NSLog(@"%@",exception);
//    } @finally {
//        return NSStringFromSelector(_cmd);
//    }
//}


#pragma mark - 关闭或开启实例变量赋值
+ (BOOL)accessInstanceVariablesDirectly{
    return YES;
}







@end
